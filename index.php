﻿<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
	<meta charset="utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link href="main.css" rel="stylesheet" />
</head>
<body>

<style type="text/css">
.nm{	
width: 360px;
height: 40px;
background-color: #ffffff;
border: 1px solid rgba(0,0,0,0.4);
}
.onmon{
	margin-right: 10px;
}
 @media (max-width: 600px) {
    .onmon {margin-right: 0px;}


</style>
	<div class = "head">
		<div class="text-center">
			<div class="my-team"> 
				<div class="my-team-h1 text-center" >
					Моя СУПЕР КОМАНДА
				</div>
				<div>
					<div class="my-team-border-one "></div>
				</div>
				<div>
					<div class="my-team-border-two"></div>
				</div>
				
			</div>

			<div class="container ">	
				<div class="row justify-content-center">
					<?php
						include ("bd.php"); //подключение к базе данных

						$mas = array(array(),array(),array(),array());
						$sql = mysqli_query($db, "SELECT id, img, name, title, dater FROM 3_images"); 
						// Выбор из базы данных полей id и img

						@$row = mysqli_fetch_array($sql);
						
						if (!$sql)
						{
							echo mysqli_error();
						}
						if (mysqli_num_rows($sql) > 0)
						{
							
							$i=0;
							do
							{
								$mas[0][$i] = $row['img'];
								$mas[1][$i] = $row['name'];
								$mas[2][$i] = $row['title'];
								$mas[3][$i] = $row['dater'];
								$i++;
							}
							while ($row = mysqli_fetch_array($sql));
						}
						$json = json_encode($mas);
					?>
					
					<script type="text/javascript"> 

						//Массив, в котором содержатся ссылки на объекты 
						var imageArray = JSON.parse('<?php echo $json; ?>');

						//Привязываем количество объектов к размеру массива 
						var imageCount = imageArray[0].length; 

						var LastUsed=0;

						function Heroes(num)
						{
							var N = 4*num;
							for(var i=0;i<4;i++,N++)
								if(N<imageCount)
								{

									document.getElementById('block'+i).innerHTML = (

										'<div class="rounded-circle" style="margin:auto;margin-bottom:20px;background:url(\'img/'+imageArray[0][N]+'\') no-repeat center center; background-size:cover;" alt="Generic placeholder image;"></div>'+
										'<div class="hero-name">'
											+imageArray[1][N]+
											'<br>'+
											'<span>'
												+imageArray[2][N]+
												'<p class="hero-date">'+
													'Дата вступления в команду:<br>'
													+imageArray[3][N]+
												'</p>'+
											'</span>'+
										'</div>');
									document.getElementById('block'+i).style.display = '';
								}
								else
									document.getElementById('block'+i).style.display = 'none'; 
						}
						
						function Butens(num)
						{
							for(var i=0;i*4<imageCount;i++)
							{
								document.getElementById('buten'+i).style.boxShadow = '0px 0px 7px 3px rgba(255,255,255,.2)';
							}
							document.getElementById('buten'+num).style.boxShadow = '0px 0px 15px 5px rgba(63,255,63,.8)';
							Heroes(num);
						}
					

						for(var i=0;i<4;i++)
							document.write('<div id="block'+i+'" class="col-lg-3"> </div>');
						
						document.write('</div>');

						for(var i=0;i*4<imageCount;i++)
							document.write('<div id="buten'+i+'" class="budd" onclick="Butens('+i+')"></div>');
						
						Butens(0);
						</script> 

				</div>
			</div>
		</div>
	</div>

	<div class="hero-mid text-center">
		<div class="my-team"> 
			<div class="add-hero-text">
				Добавь своего <span>Героя</span>
			</div>
			<div>
				<div class="my-team-border-one "></div>
			</div>
			<div>
				<div class="my-team-border-two"></div>
			</div>
		</div>
	</div>
	<div class="forms-add-foto">
		<div class="form-wrap" style="margin-bottom: 100px;">
			<div class="profile"></div>
	  		<div class="text-center">
		 		<form method="post" class="justify-content-center text-center" action="add_images.php" enctype='multipart/form-data' >
					<div class="navbar justify-content-center text-center">
						<div class="onmon">
					

			    			<div class="text-left">
			     				<label for="text" >Имя<span style="color:red">*</span></label>
			    			
			    			</div>

			    			<input type="text" name="name" class="nm" maxlength="10"C required>
			    		</div>
			    		
			    		<div>
						    <div class="text-left">
						    	<label for="text" >Описание<span style="color:red">*</span></label>
						    </div>
						    <input type="text" name="title" class="nm" maxlength="15"   required>
						</div>
			    	</div>

					<label>
						<br>
						<span>Фото <span style="color: red;">*</span></span>
						<input type='file' name='myfile'>
					</label>
					<br>
					<br>
					<div style="max-width: 750px;margin:auto;  ">
						<div class="text-right">
						<input type='image' src="rer.PNG" style = "width:165px; height:40px;"
						title='Сохранить'> 

					</div>

					</div>
					
		  		</form>
	  		</div> 
		</div>
	</div>
		<div class="futer" style="background-color: #272727; min-height: 100px;width: auto;">
			<div class="row justify-content-between">
				<div class="col-4">
				<p class="foot text-left">ALL RIGHTS RESERVED. COPYRIGHT <b style="" > © CKDIGITAL</b></p>	
				</div>

				<div class="col-3">
				<img src="https://png.icons8.com/ios-glyphs/50/000000/facebook.png">
				<img src="https://png.icons8.com/ios/50/000000/twitter-circled-filled.png">
				<img src="https://png.icons8.com/ios/50/000000/instagram-new.png">
				<img src="https://png.icons8.com/ios/50/000000/google-plus.png">
				</div>	
						
			</div>

		</div>
			
		
	
<style type="text/css">
.soc{

width: 31px;
height: 31px;
border-radius: 16px;
background-color: #b0b0b0;

}
.foot{
margin-left: 20%;
font-size: 12px;
letter-spacing: 0px;
line-height: 54px;
color: rgb(176,176,176);
font-family: "Open Sans";
text-align: center;

}

.budd{
	position: center;
	cursor: pointer;
	outline: none;
	display: inline-block;
	padding: 5px 5px;
	margin: 20px 20px;
	border-radius: 4px;
	background-image: linear-gradient(-20deg, #000000 -10%, #C0C0C0 51%, #000000 110%);
	background-size: 180% auto;
	transition: .5s;
}
.budd:hover {
	background-position: right center;	
}
.bord{
  cursor: pointer;
  text-decoration: none;
  outline: none;
  display: inline-block;
  color: black;
  padding: 20px 3px;
  margin: 0px 0px;
  font-family: 'Montserrat', sans-serif;
  text-transform: uppercase;
  letter-spacing: 0px;
  background-image: linear-gradient(-20deg, #00FF00 0%, #C0C0C0 51%, #00FF00 100%);
  background-size: 100% auto;
}


</style>
	

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>	
</body>